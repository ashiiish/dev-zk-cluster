FROM java:8

EXPOSE 2181
EXPOSE 2888
EXPOSE 3888

RUN mkdir -p /opt/zk; wget http://apache.claz.org/zookeeper/zookeeper-3.4.11/zookeeper-3.4.11.tar.gz -O /tmp/zk.tgz; \
        tar xvf /tmp/zk.tgz --strip-components=1 -C /opt/zk

WORKDIR /opt/zk

ENTRYPOINT [ "/opt/zk/bin/zkServer.sh", "start-foreground" ]
